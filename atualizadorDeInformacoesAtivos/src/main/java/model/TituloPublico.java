package main.java.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity 
@Table(name="titulo_publico")
public class TituloPublico {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id_titulo")	
	private Long idTitulo;
	
	@Column(name="titulo")
	private String title;
	
	@Column(name="data_vencimento")
	private String dueDate;
	 
	@Column(name="taxa_retorno")
	private float rateReturn;
	
	@Column(name="valor_minimo")
	private float minimumValue;
	
	@Column(name="valor_unidade")
	private float unitPrice;
	
	
	public TituloPublico(){
		
	}


	public Long getIdTitulo() {
		return idTitulo;
	}

	public void setIdTitulo(Long idTitulo) {
		this.idTitulo = idTitulo;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDueDate() {
		return dueDate;
	}

	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}

	public float getRateReturn() {
		return rateReturn;
	}

	public void setRateReturn(float rateReturn) {
		this.rateReturn = rateReturn;
	}

	public float getMinimumValue() {
		return minimumValue;
	}

	public void setMinimumValue(float minimumValue) {
		this.minimumValue = minimumValue;
	}
	
	public float getUnitPrice() {
		return unitPrice;
	}
	
	public void setUnitPrice(float unitPrice) {
		this.unitPrice = unitPrice;
	}	
}
