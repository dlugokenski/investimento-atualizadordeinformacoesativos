package main.java.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity 
@Table(name="cotacao")
public class Cotacao {
	
	@Id
    @GeneratedValue
    @Column(name="idcotacao")	
	private Long idCotacao;
	
	@Column(name="ativo")
	private String ativo;
	
	@Column(name="preco")
	private String preco;
	
	@Column(name="ultimaatualizacao")
	private String data;
	
	public Cotacao() {
		
	}
	
	public Cotacao(String ativo, String preco, String data) {
		this.ativo = ativo;
		this.preco = preco;
		this.data = data;
	}

	public String getAtivo() {
		return ativo;
	}

	public void setAtivo(String ativo) {
		this.ativo = ativo;
	}

	public String getPreco() {
		return preco;
	}

	public void setPreco(String preco) {
		this.preco = preco;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public Long getIdCotacao() {
		return idCotacao;
	}

	public void setIdCotacao(Long idCotacao) {
		this.idCotacao = idCotacao;
	}		
	
}
