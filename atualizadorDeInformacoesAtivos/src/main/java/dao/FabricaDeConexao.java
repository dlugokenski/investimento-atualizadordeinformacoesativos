package main.java.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class FabricaDeConexao {
	public static Connection getConnection() throws SQLException {
		try {
			Class.forName("org.postgresql.Driver"); // Altere o Driver caso não
													// seja o MySQL
			return DriverManager.getConnection("jdbc:postgresql://localhost:5432/Cotacao", "kleiton", "dlugokenski");
		} catch (ClassNotFoundException e) {
			throw new SQLException(e.getMessage());
		}
	}
	
	public static EntityManager buscarEntidadeManager(){
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("cotacao");
		EntityManager manager = factory.createEntityManager();
		return manager;
	}

}
