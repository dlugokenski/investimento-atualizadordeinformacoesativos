package main.java.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import main.java.model.Cotacao;
import main.java.model.TituloPublico;

public class Dao {

	public ArrayList pegarAtivos()
			throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
		Connection conexao = FabricaDeConexao.getConnection();
		ArrayList<String> listaDeAtivos = new ArrayList<>();
		String ativo = "";

		String querySql;

		querySql = "SELECT ativo " + "FROM  public.cotacao ";

		PreparedStatement stmt = conexao.prepareStatement(querySql);

		ResultSet rs = stmt.executeQuery();

		while (rs.next()) {
			ativo = rs.getString("ativo");
			listaDeAtivos.add(ativo);
		}
		rs.close();
		stmt.close();
		conexao.close();
		return listaDeAtivos;
	}

	public boolean existingAtivo(String ativo)
			throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
		Connection conexao = FabricaDeConexao.getConnection();
		boolean resultado = false;

		String querySql;

		querySql = "SELECT ativo " + "FROM  public.cotacao " + "WHERE ativo = ? ";

		PreparedStatement stmt = conexao.prepareStatement(querySql);

		stmt.setString(1, ativo);

		ResultSet rs = stmt.executeQuery();

		while (rs.next()) {
			resultado = true;
		}
		rs.close();
		stmt.close();
		conexao.close();
		return resultado;
	}

	public void cadastrarCotacao(Cotacao cotacao)
			throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
		Connection conexao = FabricaDeConexao.getConnection();
		String querySql;

		querySql = "INSERT INTO public.cotacao(ativo,preco,ultimaAtualizacao)	values(?,?,?) ";

		PreparedStatement stmt = conexao.prepareStatement(querySql);

		stmt.setString(1, cotacao.getAtivo());
		stmt.setString(2, cotacao.getPreco());
		stmt.setString(3, cotacao.getData());
		stmt.execute();
		stmt.close();

		conexao.close();
	}

	public void alterarCotacao(Cotacao cotacao) throws SQLException {
		Connection conexao = FabricaDeConexao.getConnection();
		String querySql;

		querySql = "UPDATE public.cotacao " + "SET preco = ?, ultimaAtualizacao = ? " + "WHERE ativo = ? ";

		PreparedStatement stmt = conexao.prepareStatement(querySql);

		stmt.setString(1, cotacao.getPreco());
		stmt.setString(2, cotacao.getData());
		stmt.setString(3, cotacao.getAtivo());
		stmt.execute();
		stmt.close();
		conexao.close();
	}

	public static Cotacao buscarCotacaoEmEspecifico(String nomeAtivo) {
		EntityManager manager = FabricaDeConexao.buscarEntidadeManager();
		String queryString = "SELECT * FROM cotacao WHERE ativo = :cotacao ";
		Query query = manager.createNativeQuery(queryString, Cotacao.class);
		query.setParameter("cotacao", nomeAtivo);
		return (Cotacao) query.getSingleResult();
	}

	public static TituloPublico buscarTituloPublico(TituloPublico tituloPublicoParam) {
		EntityManager manager = FabricaDeConexao.buscarEntidadeManager();
		String queryString = "SELECT * FROM titulo_publico WHERE titulo = :titulo ";
		TituloPublico tituloPublico = (TituloPublico) manager.createNativeQuery(queryString, TituloPublico.class)
				.setParameter("titulo", tituloPublicoParam.getTitle()).getSingleResult();
		return tituloPublico;
	}

	public static boolean verificarSeTituloPublicoExiste(TituloPublico tituloPublicoParam) {
		EntityManager manager = FabricaDeConexao.buscarEntidadeManager();
		String queryString = "SELECT count(*) > 0 FROM titulo_publico WHERE titulo = :titulo ";
		return (boolean) manager.createNativeQuery(queryString).setParameter("titulo", tituloPublicoParam.getTitle())
				.getSingleResult();
	}

	public static void atualizarTituloPublico(TituloPublico titulo) {
		EntityManager manager = FabricaDeConexao.buscarEntidadeManager();
		manager.getTransaction().begin();
		manager.merge(titulo);
		manager.getTransaction().commit();
		manager.close();
	}

	public static void cadastrarTituloPublico(TituloPublico titulo) {
		EntityManager manager = FabricaDeConexao.buscarEntidadeManager();
		manager.getTransaction().begin();
		manager.persist(titulo);
		manager.getTransaction().commit();
		manager.close();
	}
}
