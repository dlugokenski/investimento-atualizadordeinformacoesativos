package main.java.run;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import com.google.gson.Gson;

import main.java.dao.Dao;
import main.java.model.TituloPublico;

public class TesouroDireto {

	public static void main(String[] args) throws ParseException {
		pegarDadosApiTesouroDireto();
	}

	public static void pegarDadosApiTesouroDireto() throws ParseException {
		SimpleDateFormat formatador = new SimpleDateFormat("dd/MM/yyyy");
		ArrayList<TituloPublico> listaDeTitulosPublicos = new ArrayList<>();
		Gson gson = new Gson();
		HttpClient httpclient = HttpClients.createDefault();
		HttpGet httpget = new HttpGet("http://localhost:8080/titulos-tesouro-direto");
		try {
			HttpResponse response = httpclient.execute(httpget);
			if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				HttpEntity entity = response.getEntity();
				String content = EntityUtils.toString(entity);
				JSONObject json = new JSONObject(content);
				JSONArray  investmentSecurities = json.getJSONArray("investmentSecurities");
				
				 for(int i = 0; i < investmentSecurities.length(); i++){	
					 TituloPublico tituloPublico = gson.fromJson(investmentSecurities.getJSONObject(i).toString(), TituloPublico.class);
					 if(Dao.verificarSeTituloPublicoExiste(tituloPublico) == true){
						 TituloPublico tituloBanco = Dao.buscarTituloPublico(tituloPublico);
						 Dao.atualizarTituloPublico(tituloBanco);
					 } else {
						 Dao.cadastrarTituloPublico(tituloPublico);
					 }					
			     }
			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			httpget.releaseConnection();
		}
	}
}
