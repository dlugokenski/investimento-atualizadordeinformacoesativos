package main.java.run;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URLEncoder;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Scanner;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import main.java.dao.Dao;
import main.java.model.Cotacao;


public class CotacaoAcao {

	public static void main(String[] args) {
		executarColetasDeCotacaoDiario();
	}

	private static void getCotacao(String ativo) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
		Dao dao = new Dao();
		HttpClient httpclient = HttpClients.createDefault();
		String key = "6CKVMMQHZ8N99GBH";
		HttpGet httpget = new HttpGet(
				"https://www.alphavantage.co/query?function=TIME_SERIES_INTRADAY&symbol="+ ativo +".SA&interval=1min&outputsize=full&apikey="+key);
		try {
			HttpResponse response = httpclient.execute(httpget);
			if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				HttpEntity entity = response.getEntity();
				String content = EntityUtils.toString(entity);
				JSONObject json = new JSONObject(content);
				JSONObject metaData = json.getJSONObject("Meta Data");
				String ultimaData = (String) metaData.get("3. Last Refreshed");
				JSONObject teste = json.getJSONObject("Time Series (1min)");
				Object keyvalue2 = teste.get(ultimaData);
				JSONObject jsonFinal = new JSONObject(keyvalue2.toString());
				String valorFinalDaCotacao = jsonFinal.getString("4. close");
				
				Cotacao ativoAtual = new Cotacao(ativo, valorFinalDaCotacao, ultimaData);
				if(dao.existingAtivo(ativo)){
					dao.alterarCotacao(ativoAtual);
				} else {
					dao.cadastrarCotacao(ativoAtual);
				}
				
			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			httpget.releaseConnection();
		}
	}

	private static void getAtivosDoBanco() throws InstantiationException, IllegalAccessException, ClassNotFoundException,
			SQLException, InterruptedException {
		Dao dao = new Dao();
		ArrayList<String> listaDeAtivos = dao.pegarAtivos();
		for (int i = 0; i < listaDeAtivos.size(); i++) {
			getCotacao(listaDeAtivos.get(i));
			new Thread().sleep(14000);
		}
	}

	private static void executarColetasDeCotacaoDiario() {
		Calendar c = Calendar.getInstance();
		c.set(Calendar.HOUR_OF_DAY, 00);
		c.set(Calendar.MINUTE, 4);
		c.set(Calendar.SECOND, 0);

		Date time = c.getTime();
		final Timer t = new Timer();
		t.schedule(new TimerTask() {
			@Override
			public void run() {
				try {
					getAtivosDoBanco();
				} catch (InstantiationException | IllegalAccessException | ClassNotFoundException | SQLException
						| InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

		}, time);
	}
}
